# Demo Overview #


The purpose of the project is to demonstrate iFrame content from the Agent Console.  This iFrame content is pulled from a web app.  However, we require that the Assist iFrame SDK not be added to the web apps.  This iFrame content must then be visible to an external Customer's Client during co-browse.

## Resolution ##

We must therefore address the issue of loading content from different web apps into an iFrame from the Agent Console via e.g. an onclick of a link or some other event change - that may or may not be controlled by our customer, and may or may not be within their internal network, or may or may not be accessible for some reason or other.